---
title: 自建图床 uppy
tags: [web]
date: 2018-04-11 23:45:00
---

## 选择

自建图床的方案可以说不是很多了，先是用过 Lychee -- 使用世界上最好的语言(PHP)开发，使用下来感觉功能太重了：一堆功能，能用上的没几个。还要装一堆图像处理的插件，去做图片缩放(并用不到

目前是使用的 uppy，优点是部署方便，一个 `npm install` 就解决了，后端用的是 `tusd` 的 go 版本，部署更是方便，一个二进制文件扔上去就好了。

## 困惑 & 解决

在选择 uppy 之后，看了半天官方文档，也没有提到怎么样把 uppy 部署好，我甚至以为 uppy 是 uppy server 的后端。走投无路的时候去扒 uppy 的官方例子的页面，发现部署 uppy 只需部署一个静态页面即可，所有选项都在页面的 `<script>` 标签内写好就成。

<!--more-->

示例页面：

``` html
<html>
<head>
    <link href="/static/uppy.min.css" rel="stylesheet">
</head>
<body>
    <div>
      <div>
        <div id="dashborad-container"></div>
      </div>
    </div>
    <script src="/static/uppy.min.js"></script>
    <script>
      var uppy = Uppy.Core({ autoProceed: true })
      uppy.use(Uppy.Dashboard, { target: '#dashborad-container', inline: true, replaceTargetContent: true })
      uppy.use(Uppy.Tus, { endpoint: '/files/', uploadUrl: '/files/', overridePatchMethod: true, resume: true})
      uppy.run()

      uppy.on('complete', result => {
        console.log('successful files:', result.successful)
        console.log('failed files:', result.failed)
      })
    </script>
</body>
</html>
```

效果图：
![](/images/2018/04/uppy-screenshot-1.png)
![](/images/2018/04/uppy-screenshot-2.png)

功能很简单：上传/生成链接
