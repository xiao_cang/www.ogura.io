---
title: tmux 与 vim 开启 true-color 支持
date: 2018-09-29 11:23:02
tags: [vim]
---

受 <http://blog.acgtyrant.com/%E5%9C%A8-Linux-%E4%B8%8B%E5%85%A8%E9%9D%A2%E4%BD%BF%E7%94%A8%E7%9C%9F%E5%BD%A9.html> 这篇文章启发，知道了在终端的世界里有高于256色的True-Color的存在[^1]。
对于常年不离开终端的重度Vim用户来讲，这种多色彩的支持的吸引力也是致命的。

__目录__

<!-- toc -->

# 概览

想要真正的多色彩支持，需要 `模拟终端` + `tmux` + `vim` 三重的支持。这里说明下我的编程环境：windows 虚拟机(Ubuntu 18.04) + SecureCrt + Tmux + Vim 8.1。
于是我要做的就是在这几点上分别做True-Color支持。

# 模拟终端

我在Windows上偏爱使用的模拟终端是 SecureCrt，但，我在查阅 True-Color 支持列表时<https://gist.github.com/XVilka/8346728#now-supporting-truecolour>却发现 SecureCrt 不在支持之列。在试了几个Windows支持的虚拟终端之后，最终选定了 `MobaXTerm`。

至于原因，无外乎其与 SecureCrt 相似的操作逻辑，以及提供 Personal Edition。

至于配色偏好，我依然选了墨绿色背景的 solarized 主题。[^2]

# tmux

这里采用网上常规的方法没有行得通，经多方搜寻，找到了 <http://lists.gnu.org/archive/html/emacs-devel/2017-02/msg00635.html> 更改 terminfo 使得 tmux 支持 Tc (True-Color) 的方法。（这里我采用了git上最新的 tmux 源码编译的 tmux）

常规方法，更改 `$HOME/.tmux.conf` 文件：

``` .tmux.conf
# ！！！importent！！！ 开启24 bit color 其他方式都无效
set -g default-terminal "tmux-256color"
set -ga terminal-overrides ",*256col*:Tc"
```

更改 Terminfo 方法（实测有效）:

``` bash
/usr/bin/infocmp > /tmp/.infocmp
echo "  Tc," >> /tmp/.infocmp
/usr/bin/tic -x /tmp/.infocmp
```

校验 True-Color 的方法：

``` bash
awk 'BEGIN{
    s="/\\/\\/\\/\\/\\"; s=s s s s s s s s;
    for (colnum = 0; colnum<77; colnum++) {
        r = 255-(colnum*255/76);
        g = (colnum*510/76);
        b = (colnum*255/76);
        if (g>255) g = 510-g;
        printf "\033[48;2;%d;%d;%dm", r,g,b;
        printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
        printf "%s\033[0m", substr(s,colnum+1,1);
    }
    printf "\n";
}'
```

效果图：

![True-Color效果](/images/2018/09/true-color-in-vim.png)

# vim

这里要改的点主要有三个：

1. 按照文档中方法的更改 `.vimrc` 文件

``` .vimrc
if has("termguicolors")
    " fix bug for vim
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

    " enable true color
    set termguicolors
endif
```

2. 安装适合自己的支持真彩色的vim 主题，我这里选用的是相同配色的 `solarized8` 主题

``` .vimrc
Plugin 'lifepillar/vim-solarized8'

" 这里增强了对比度
set background=dark
colorscheme solarized8_high
let g:solarized_extra_hi_groups=1
```

3. 在进入 vim 的 VISUAL 模式后，发现选定的行不能反色显示，而仅是失去了高亮，这很容易看不清选中的具体字符（比如注释也是没有高亮的，这里就很容易找不到选中边界）。
搜寻的结果则是终端的支持问题，无法识别 `hi Visual gui=reverse` 这个反色指令，于是手动调整了被选中文字的配色。

``` .vimrc
hi Visual gui=reverse guifg=Black guibg=Grey
```
（依然在 .vimrc 文件中修改，具体位置则是紧接着主题配色后面）

before:
![before](/images/2018/09/vim-default-reverse.gif)

after:
![after](/images/2018/09/vim-custom-reverse.gif)

> 这里同样是选中 358-363 行

# 补充

在使用过程中，还发现了 vim 8.1 中支持的 termdebug 与 true-color 的一个冲突：在 `packadd termdebug` 之后做了 `set termguicolors` 的操作的话，在实际调试代码时，正在执行的行就没办法高亮了。而正确的做法则是将 `packadd termdebug` 放在 `set termguicolors` 之后。

# 意义

由 256 色支持到 16万色支持，字面意义上就是颜色过渡更平滑了。而在 vim 中，就是代码看着更舒服了（不知道这里的代码颜色高亮有没有相应的True-color扩展）

[^1]: 8-bit color 又名 256 color, 24-bit color 又名 true color，一共有 16,777,216 colors
[^2]: 与这款配色的渊源要回溯到 <https://blog.csdn.net/zklth/article/details/8937905> 这篇文章了，可惜文章中的图已经挂了大半，还好我有本地保存的 Evernote 版。
