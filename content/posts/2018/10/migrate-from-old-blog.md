---
title: 老博客文章迁移
date: 2018-10-10 19:14:00
tags: [life]
linkcolor: sumire
---

由 <http://yihui.name/cn/2018/09/countryman/> 文章中的 <http://disq.us/p/1wev2g5> 这条评论想到
> 哈哈，十三年前的日志是回忆当时三年前的事。所以就算是为了这种酷，也要坚持写博客啊。

我在不同博客平台[^1] 之间辗转过多次，也因为域名主机等问题放弃维护了一些文章，而目前可查的文章居然是 2013-2014 年在 github 上留底的 jekyll 搭建博客期间的文章。算一算竟也是 5年前了。

动手把文章迁移过来，以增加博客的历史厚重感，可以在博客的页脚看到 `since 2013` 了 :)

[^1]: Wordpress -> jekyll -> bitcron -> hexo
