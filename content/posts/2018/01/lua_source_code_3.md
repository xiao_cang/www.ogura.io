---
title: lua 源码阅读(三)
tags: [lua]
date: 2018-01-09 19:36:00
---

>
lua 源码系列文章:
- [lua 源码阅读计划](/posts/2017/12/lua_source_code_plan/)
- [lua 源码阅读(一)](/posts/2017/12/lua_source_code_1/)
- [lua 源码阅读(二)](/posts/2017/12/lua_source_code_2/)
- lua 源码阅读(三)
- [lua 源码阅读(四)](/posts/2018/01/lua_source_code_4/)
- [lua 源码阅读(五)](/posts/2018/01/lua_source_code_5/)
- [lua 源码阅读(六)](/posts/2018/01/lua_source_code_6/)

lua源码欣赏的第四章到4.2.1章的内容，主要看了与 lua 表相关的源码。

__Contents__
<!-- toc -->

## Lua Table

一个lua的表结构，最多由三块连续的内存组成：一个 Table 结构，一个存放了连续整数索引的数组，一块大小为2的整次幂的哈希表。而每一部分的大小，在调用 `rehash` 函数时，会由 `computesize` 函数在保证利用率在 50% 以上的前提下重新分配。
哈希表使用哈希的闭散列的冲突解决方法，在发生碰撞后会将碰撞的两个key用单链表链接起来。

> [散列算法详解](https://www.jianshu.com/p/f9239c9377c5)

<!--more-->

## Hash

而在表的查询过程中，也遇到了长字符串的惰性哈希计算。这点优化的原因，云风在书里也有所说明:

> 在大多数应用场合，长字符串都是文本处理的对象，而不会做比较操作，内部唯一化处理将带来额外开销

但字符串优化是在 Lua 5.2.0 之后才加入的新特性，是否在 openresty 中配套的 luajit 中也有，先打个问号。

> <https://github.com/xiaocang/lua-5.2.2_with_comments/releases/tag/lua_table_04>
