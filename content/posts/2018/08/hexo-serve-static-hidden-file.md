---
title: hexo + gitlab 服务隐藏静态文件
date: 2018-08-01 20:42:12
tags: [hexo]
---

前情提要
========

由于之前一直托管博客的网站 <https://bitcron.com/> 无法更新证书，而该网站的 Let's Encrypt 自动脚本又挂掉不能用了（这网站该不会凉了吧(小声）。我决定抽空把博客迁移到纯静态博客上。

之前有过使用 hexo 的经验，又加上 node 的生态在个人博客这里又异常繁荣（大量node相关的第三方插件可用），还是决定迁移过来了。

在 `Github` 被微软收购后，虽然其CEO直接升级成为 Billionaire，但我对他的好感降到了冰点，目前仅留了几个开源的小项目和之前fork的项目在上面，私有库的付费计划已经停止了。现在把大部分代码及私有库都转移到了 gitlab 上。于是使用 gitlab pages + hexo 的初步技术选型就已经定下来了。

<!-- more -->

顺利进行
========

首先，跟着 hexo 官方的文档，一步一步安装，将大的框架搭起来。再配合 GitLab CI 来自动生成及部署静态页面。一切看起来异常的简单。

遇到问题
========

在 gitlab 上的 pages 选项中，很容易就找得到绑定域名和给域名设置证书的选项。在vps上安装 Let's Encrypt的命令行工具 `certbot`，
按照官方的文档：

``` bash
certbot certonly --manual -d www.ogura.io
```

下一步，下一步，直到停在需要验证网站所有权的地方：

访问 `http://www.ogura.io/.well-known/acme-challenge/JDbJQP50-rI3LgKGLy7U7EhOkSxr73P0bP2UeTIh1yE` 得到指定内容的静态文件。

在自己的 VPS 上做静态文件服务再简单不过了，不过如果是在 gitlab + hexo 的情形下又该怎么做呢。

经过试验得知，在 `source/` 文件夹下的文件会被直接扔到 `public/` 文件夹下，但特殊情况是，`.well-known` 文件夹是个隐藏文件夹，
`hexo generate` 在生成静态文件的时候，会忽略隐藏文件和隐藏文件夹。

gitlab pages 中举例了一个 jekyll 中的解决方案是使用一个 `xxx.md` 的文件，其内容为：

``` markdown
---
layout: null
permalink: /.well-known/acme-challenge/5TBu788fW0tQ5EOwZMdu1Gv3e9C33gxjV58hVtWTbDM
---

5TBu788fW0tQ5EOwZMdu1Gv3e9C33gxjV58hVtWTbDM.ewlbSYgvIxVOqiP1lD2zeDKWBGEZMRfO_4kJyLRP_4U
```

但是在 hexo 中（起码是 `next` 这个主题下，没有 `null` 这个 layout），而且生成的链接中带有 `.html` 后缀，无法通过校验。

折腾了半天自定义layout，也没弄成功。
最终在 `.gitlab-ci.yml` 中，加了一行手动 copy 的命令，也就成功了。（折腾了半个小时layout模版= =）

``` yml
# This file is a template, and might need editing before it works on your project.
# Full project: https://gitlab.com/pages/hexo
image: node:6.10.0

pages:
  script:
  - npm install
  - ./node_modules/hexo/bin/hexo generate
  - /bin/cp -rv static/.well-known public/
  - ./node_modules/.bin/hexo algolia
  artifacts:
    paths:
    - public
  cache:
    paths:
      - node_modules
    key: project
  only:
  - master
```
