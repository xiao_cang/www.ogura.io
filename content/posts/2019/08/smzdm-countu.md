---
title: countu 游泳计数器 (安利)
date: 2019-08-12T22:32:00+08:00
tags: [life, swimming, shopping]
linkcolor: sumire
---

前言
====

在学会[自由泳](/posts/2019/07/learn-to-crawl/)后，小米手环在使用过 8 次之后光荣的出故障了。

![mi-band-crash](/images/2019/08/mi-band-crash.jpg)

选项
====

于是在 JD 和 TB 上寻找靠谱的替代品：

1. Moov Now 游泳智能手环：￥399.00
2. Swimovate PoolMate2 标准版：￥790.00
3. Swimovate PoolMate Live 灵动版 + Swimovate PoolMate Live灵动版USB数据夹 ：￥945.00 + ￥335.00
4. GARMIN Forerunner735：￥2280.00
5. COUNTU 2018：￥ 288.17
6. SportCount：￥ 238.00

最终的选择如标题所示，COUNTU 2018

![countu-2018](/images/2019/08/countu-2018.png)

理由
====

Moov Now 本是首选，不过在看了几篇测评之后，发现其测试精准度依然一般。因为有小米手环的前车之鉴，就 PASS 掉了。

Swimovate 的两款单独列出来是因为价格和功能，在理论上，这两款表的测量精准度达到了专业级别，Live 版多了一个数据导出的功能，但是要配合买一个 USB 数据夹。只考虑测量准确性的话，买 Swimovate PoolMate2 标准版就足够了。待定。

GARMIN Forerunner735 是专业的铁人三项用表了，只是价格不太美丽，PASS。

还剩两款非手环形态的选项：COUNTU 和 SportCount，这两款其实是在全网能买到的指套型计数器中针对游泳的唯二选择。其中 SportCount 的操作和显示据说比较蠢，而 COUNTU 作为一个国产品牌做了很多人性化的小优化。

最终就是在 Swimovate 和 COUNTU 中选择了一个更硬核更便宜的一个：COUNTU。
