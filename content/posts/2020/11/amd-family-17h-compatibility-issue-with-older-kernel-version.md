---
title: 旧版本内核与 AMD Family 17h 系列 CPU 的兼容性问题
date: 2020-11-13T17:20:00+08:00
tags: [kernel]
---

最近同事在我司某底层软件在各版本内核上的兼容性时，遇到了诡异的问题：
使用 kvm 启动特定版本内核的 `CentOS 7` 的时候，发生了致命错误，导致系统无法启动。

这些内核的版本号如下：

- `3.10.0-123`
- `3.10.0-229`

TL;DR
=====

这里其实是一个 AMD Family-17h 架构的 CPU 与较老版本的内核之间的一个兼容性 bug，
该 bug 已经在更新版本的内核中修复：

https://git.kernel.org/pub/scm/linux/kernel/git/tip/tip.git/commit/?id=e40ed1542dd779e5037a22c6b534e57127472365

解决过程
========

LVM ?
-----

kvm 中的操作系统无法启动后，使用 vnc 连接上该机器，发现报错：

``` bash
dracut-initqueue timeout and could not boot – warning /dev/centos/root-lv does not exist
```

这个错误我见过很多次，在搜索引擎输入关键字也能找到一堆解决方法。不过大都是 UUID 
对不上导致的错误，我这里是 LVM，所以我怀疑是内核启动时不识别 lvm 的分区导致的系统
无法启动。

然后我尝试使用标准分区来代替 LVM，重装系统，报错依旧，无法启动。

XFS/EXT4/EXT2 ?
---------------

这时我开始怀疑 boot 分区的文件系统，重装后的系统使用的是 `XFS`，而我印象中很老
版本的内核只支持 ext2 / ext3 / ext4 的文件系统，于是我又重装了两次操作系统，`/boot`
分区分别使用 `ext4` 和 `ext2`，依然没有解决问题。

GRUB2 ?
-------

这时我又开始怀疑 grub 2 的参数问题，由于在使用标准分区后，指定启动硬盘的语句就变成了
`linux16 ... root=UUID=xxxx`，这里的 `root=UUID=xxx` 是不是也有可能是老的内核版本
不支持的呢？于是我在 `/etc/default/grub` 文件中加入了 `GRUB_DISABLE_LINUX_UUID=true`，
再执行：

``` bash
# 注意，kvm 没有用 efi 启动
grub2-mkconfig -o /boot/grub2/grub.cfg
```

更新之后，再重启试试

...

还是不行

VIRTIO ?
--------

我开始怀疑内核启动后压根没找到设备，于是开始找 kvm 的疑点：virtio。virtio 是 kvm 提供
的硬盘总线 (bus)，应该需要对应的驱动才行，于是我执行 `lsinitrd` 命令来查找是否有
对应的驱动：

``` bash
# sudo lsinitrd /boot/initramfs-3.10.0-229.7.2.el7.x86_64.img | grep virtio

-rw-r--r--   1 root     root        27885 Jun 24  2015 usr/lib/modules/3.10.0-229.7.2.el7.x86_64/kernel/drivers/block/virtio_blk.ko
-rw-r--r--   1 root     root        52861 Jun 24  2015 usr/lib/modules/3.10.0-229.7.2.el7.x86_64/kernel/drivers/char/virtio_console.ko
-rw-r--r--   1 root     root        50501 Jun 24  2015 usr/lib/modules/3.10.0-229.7.2.el7.x86_64/kernel/drivers/net/virtio_net.ko
-rw-r--r--   1 root     root        29125 Jun 24  2015 usr/lib/modules/3.10.0-229.7.2.el7.x86_64/kernel/drivers/scsi/virtio_scsi.ko
drwxr-xr-x   2 root     root            0 Nov 13 16:19 usr/lib/modules/3.10.0-229.7.2.el7.x86_64/kernel/drivers/virtio
-rw-r--r--   1 root     root        15797 Jun 24  2015 usr/lib/modules/3.10.0-229.7.2.el7.x86_64/kernel/drivers/virtio/virtio.ko
-rw-r--r--   1 root     root        21253 Jun 24  2015 usr/lib/modules/3.10.0-229.7.2.el7.x86_64/kernel/drivers/virtio/virtio_pci.ko
-rw-r--r--   1 root     root        25541 Jun 24  2015 usr/lib/modules/3.10.0-229.7.2.el7.x86_64/kernel/drivers/virtio/virtio_ring.ko
```

结果是：驱动找得到，和可正常启动的较新版内核没差别。

AMD Family-17h !
----------------

这时还有一个疑点，就是开机时一闪而过的一句提示（我重启了很多次，最终是截图才看清这句话）：`core perfctr but no constraints; unknown hardware!`

用这个关键词，Google 查了一下，结果豁然开朗：这个是 AMD Family-17h 与较老版本内核之间的一个兼容性问题。可以引用下面这句话解释：

> In family-17h, there is no PMC-event constraint. All events, irrespective of
> the type, can be measured using any of the six generic performance counters.

而这里的 AMD family-17h 对应的是 zen / zen+ / zen2 架构的 CPU，和正在使用的这台
虚拟机母机对应得上，也能刚好解释之前在 Intel 平台上可以正常使用该版本内核启动
机器。

当然这个问题的解决办法就是升级内核，在几年前各大操作系统已经应用了这个修复。
