---
title: Yubikey 的 polkit 配置
date: 2020-04-14T10:13:00+08:00
tags: [yubikey]
toc: true
---

由于工作中有存储 GPG 密钥的需求，又不是很想把密钥存在云服务或者 Git 服务中，去年 2 月购买了 Yubikey5 Nano 版。

总体下来还是很满意的，虽然在使用过程中确实遇到些问题，也基本 Google 解决了。

当时还打算写一下相关踩坑教程，可惜都搁置了，这篇文章基本能覆盖你碰到的大部分问题：<https://mechanus.io/ke-neng-shi-zui-hao-de-yubikey-gpg-ssh-zhi-neng-qia-jiao-cheng/>

这篇主要说一下 Yubikey 和 polkit 工具集之间配合的坑：

重现
====

环境：fedora 30 / 31 （两台虚拟机都遇到过）

插入 Yubikey，非 root 用户执行 `gpg2 --card-status` 发现没有找到设备，提示：

``` log
gpg: selecting card failed: No such device
gpg: OpenPGP card not available: No such device
```

执行 `journalctl -xe` 发现有类似以下的日志：

``` log
pcscd[13141]: 00000000 ifdhandler.c:150:CreateChannelByNameOrChannel() failed
pcscd[13141]: 00000071 readerfactory.c:1105:RFInitializeReader() Open Port 0x200000 Failed (usb:1050/0407:libudev:0:/dev/bus/usb/003/006)
pcscd[13141]: 00000004 readerfactory.c:376:RFAddReader() Yubico YubiKey OTP+FIDO+CCID init failed.
pcscd[13141]: 00004720 ifdhandler.c:150:CreateChannelByNameOrChannel() failed
pcscd[13141]: 00000023 readerfactory.c:1105:RFInitializeReader() Open Port 0x200000 Failed (usb:1050/0407:libudev:1:/dev/bus/usb/003/006)
pcscd[13141]: 00000004 readerfactory.c:376:RFAddReader() Yubico YubiKey OTP+FIDO+CCID init failed.
pcscd[13141]: 00143849 auth.c:135:IsClientAuthorized() Process 13120 (user: 1000) is NOT authorized for action: access_pcsc
pcscd[13141]: 00000140 winscard_svc.c:335:ContextThread() Rejected unauthorized PC/SC client
```

分析
====

仔细分析这个日志可以看出来，这是一个异常调用栈的输出，而这里的症结是 `user: 1000` 没有 `access_pcsc` 权限。

通过查看 [polkit 文档](https://wiki.archlinux.org/index.php/Polkit_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))，我们知道，polkit 是为了使低优先级进程 (pcscd) 和高优先级进程(驱动程序)通信而设计出的权限管理工具集。

解决
====

想要授予权限，需要新增配置文件 `051-org.debian.pcsc-lite.rules` (文件名可自定义，以 `.rules` 结尾即可) 到 `/etc/polkit-1/rules.d/` 目录。
其中 050 为默认规则，050 之后为自定义规则，050 之前是需要对默认规则的补充

配置文件如下：

```
polkit.addRule(function(action, subject) {
    // 这里我把 wheel 组的用户都赋予了 access_pcsc 的权限
    if (action.id == "org.debian.pcsc-lite.access_pcsc" &&
        subject.isInGroup("wheel")) {
        return polkit.Result.YES;
    }
});

polkit.addRule(function(action, subject) {
    // ditto ...
    if (action.id == "org.debian.pcsc-lite.access_card" &&
        subject.isInGroup("wheel")) {
        return polkit.Result.YES;
    }
});
```

调试技巧
========

polkit 配置文件的语法和 JS 十分相似，这里可以使用 polkit.log 来做调试输出，其他的方法和说明在 polkit 文档中说的很清楚[^1]。

调试过程中需要多注意日志，如果出现 `Error compiling script` 这样的关键字，就说明有编译错误，此次更改不会生效。

[^1]: [Polkit 文档](https://wiki.archlinux.org/index.php/Polkit_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))
