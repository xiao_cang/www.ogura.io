---
title: Perl 的一致性hash实现
date: 2017-12-16 19:08:00
tags: [perl]
---

openresty 中有使用 lua 和 c 实现了和 nginx 相同结果的 一致性hash，用于在 openresty 中实现与 nginx 相同的一致性hash结果。
而在perl中没有这样的module，于是我参照 lua 和 c 的代码（基本是像素级 copy），在 perl 中实现了结果一样的一致性hash（测试用例校验通过）

项目地址在: <https://github.com/xiaocang/perl-balancer>

TODO: 暂时还没有 XS 版的实现
