---
title: lua 源码阅读(一)
date: 2017-12-16 13:00:00
tags: [lua]
layout: lua_source_code
---

>
lua 源码系列文章:
- [lua 源码阅读计划](/posts/2017/12/lua_source_code_plan/)
- lua 源码阅读(一)
- [lua 源码阅读(二)](/posts/2017/12/lua_source_code_2/)
- [lua 源码阅读(三)](/posts/2018/01/lua_source_code_3/)
- [lua 源码阅读(四)](/posts/2018/01/lua_source_code_4/)
- [lua 源码阅读(五)](/posts/2018/01/lua_source_code_5/)
- [lua 源码阅读(六)](/posts/2018/01/lua_source_code_6/)

__Contents__
<!-- toc -->

## 内存管理

刚刚读完 风云关于lua 的内存管理部分的理解，自己也对相应部分代码做了阅读和注释。
lua的内存管理主要特点如下：

1. lua 虚拟机在初始化时会在第一块申请的内存上保存主进程和全局状态机，而在之后的过程中避免内存碎片化
2. lua 通过封装的内存管理函数，在申请、调整、释放内存的时候都会有准确的内存原始大小，从而节省了如标准库中的 cookie 一样存放原始内存信息的内存
3. lua 封装了可以处理变长数组的内存管理函数，为lua语言中的数据结构的灵活性提供了保障

<!--more-->

> <https://github.com/xiaocang/lua-5.2.2_with_comments/releases/tag/lmem_01>

## 初始化

而lua的全局状态机这部分则是因为牵涉后面的部分太多，如 gc, string, table 等，只能看懂一点。
其中需要重点理解的部分是：

- lua 在初始化时，主线程的数据栈和其他现成的数据栈创建方式不同
- 全局状态机中的 buff 用来处理 lua 中的字符串操作
- lua 实现了一个版本检查功能来防止多重载入的问题，这也说明了lua在设计的初衷就是嵌入式语言
- lua 为了虚拟机的健壮，在每次内存申请时需要判断内存申请的成功与否

> <https://github.com/xiaocang/lua-5.2.2_with_comments/releases/tag/global_state_02>
