---
title: MVC 结构在生产环境中的错误使用
tags: [web]
date: 2017-12-21 11:33:00
toc: false
---

手头有一个对外的接口，采用了 Mojolicious 框架，主要实现一些通用和客户定制需求。在需求不断增加的情况下变得臃肿不堪，在这里作为一个反面用例来分析一下。
大致设计结构如下：

<!--more-->

``` flow
api=>start: API
route=>condition: Route
common_route=>subroutine: Common Route
custom_route=>subroutine: Custom Route
common_controller=>condition: Common Controller
custom_controller=>subroutine: Custom Controller
hacked_code=>operation: hacked code
hacked_middleware=>operation: hacked middleware
common_io=>inputoutput: Common io/db process
hacked_user_io=>inputoutput: Hacked io/db process
render=>end: Render

api->route(no)->custom_route->custom_controller->render
api->route(yes)->common_route->common_controller->common_io->render
api->route(yes)->common_route->common_controller(no)->hacked_code->hacked_user_io->render
api->route(yes)->common_route->common_controller(yes)->hacked_middleware->hacked_user_io->render
```

> 这里还没画出这个结构中的 Model 部分，因为也是一团乱。

简单的说明下这个被滥用的结构。

## 理想
路由主要分为两部分，一部分是通用功能的接口路由，一部分是用户定制的接口路由。这是比较理想的情况：在用户没有定制功能时，就直接使用通用路由的接口；而在需要定制功能的时候，就单独在定制路由中加新的路径。

## 现实
在实际使用中，发现这并不算是通用解决方案。因为有些用户在使用通用通用接口时，有时会有定制化需求，如：

- 在POST操作时，不调用通用的随机函数生成串，而是采用客户定制的规则生成串，再进行数据库操作。于是我在代码中加入了丑陋的uid判断逻辑，满足条件就进行A操作，否则就进行通用操作。这样多次类似的代码hack之后，我加入了一个中间件做uid判断，但依然是丑陋不堪。
- 在 `Common Route` 部分，要经过一个鉴权的中间件来确保合法性。在这里又有客户定制的鉴权方案要做。
- 在用户定制功能中，可能有新的数据需要存储起来，于是我会在 `Model` 里又创建新的表或者字段，而这个定制字段又不能很好的做访问控制。

总之，这个架构中在设计之初，有些模块或者架构就是固定的，而在实际应用的时候，几乎要求每个模块都可以被定制和替换(用户级别)。

## 解决方案(待续)
