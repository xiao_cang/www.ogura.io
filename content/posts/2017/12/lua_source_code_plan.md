---
title: lua 源码阅读计划
tags: [lua]
date: 2017-12-15 15:36:00
---

> lua 源码系列文章:
> - lua 源码阅读计划
> - [lua 源码阅读(一)](/posts/2017/12/lua_source_code_1/)
> - [lua 源码阅读(二)](/posts/2017/12/lua_source_code_2/)
> - [lua 源码阅读(三)](/posts/2018/01/lua_source_code_3/)
> - [lua 源码阅读(四)](/posts/2018/01/lua_source_code_4/)
> - [lua 源码阅读(五)](/posts/2018/01/lua_source_code_5/)
> - [lua 源码阅读(六)](/posts/2018/01/lua_source_code_6/)

在 openresty 项目中使用 lua 是一件很享受也很纠结的过程。lua 的极简设计让我两年前仅用几天搭建出来一套 CDN 控制系统，而在业务系统渐渐成型的情况下暴露出问题。 如今摆在我面前的解决方案有两个：

1. 使用状态机，将框架的核心部分改用状态机实现，将之前的控制流程改为一个个状态
2. 使用沙盒，抽象新的阶段。在每一阶段中使用设定好的沙盒环境做规定的事情。

如果要使用状态机，则可能极大的复杂化系统的核心部分，导致后期无法维护。 而要使用沙盒，问题是我对lua的底层实现一知半解，在看到风云的Blog后， 我决定抽出时间将lua的源码阅读一遍，加深我对lua的理解，在阅读中我也希望能想出 openresty 项目的理想 lua 架构。

<!--more-->

在这里附一下云风的 [Lua 源码欣赏](https://www.codingnow.com/temp/readinglua.pdf)

我会在这里更新一些阅读 Lua 源码后的感想和学到的知识。

这是我在 github 上对lua-5.2.2 源码加入中文注释后的项目地址： [https://github.com/xiaocang/lua-5.2.2_with_comments](https://github.com/xiaocang/lua-5.2.2_with_comments)
