---
title: NexT & izhengfan 结合主题
date: 2018-09-12 17:49:19
tags: hexo
---

一直很眼馋 `yinwang.org`[^1] 的超简主题，看起来就像某种高级存档。在找了相关的 hexo 主题 `mickeyouyou/yinwang`[^2] 并试用了之后，发现并不满意（主要不满意字体，排版和代码高亮）。
而 Hexo 用户基本都知道 `NexT` 主题，定制功能强大，找到适合的排版、字体高亮、第三方插件很容易，可无奈太多人在用了，而且显得有些重。

在搜索 `vim termdebug` 问题时，无意中发现了 <https://fzheng.me> 的博客，其极简的思路和漂亮的字体让我特别想试用一下相关主题。去 github 上看了相关源码是 `Jekyll` 的模版，花了点时间转换成 `hexo` 的模版，在转换的过程中，顺手把几个 `NexT` 主题中用的顺手的扩展功能移植过来了（因为只有三个页面模版，工作量很小）。

移植过来的几个功能是：
- disqus 评论。我这里做了改造，选择 lazyload 的话，不是滚动触发，而是点击按钮触发（原谅我不会做按钮的 hover 效果）
- baidu / google / bing 等网站的认证及统计功能
- 社交网络按钮
- 网站采用的协议
- 自定义网站页脚

主题项目地址是：<https://github.com/wukra/izhengfan.github.io.git>

[^1]: 王垠博客地址：<http://www.yinwang.org/>
[^2]: 仿王垠博客主题的项目地址： <https://github.com/mickeyouyou/yinwang>
