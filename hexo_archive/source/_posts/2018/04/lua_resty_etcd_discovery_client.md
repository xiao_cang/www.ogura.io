---
title: lua-resty-etcd-discovery-client
tags: lua
date: 2018-04-01 17:45:00
---

在微服务的架构中，服务发现是必不可少的一环。在采用 etcd 做服务注册和发现的情况下，多种框架、多钟语言以及不同的网络环境下的服务注册变成了重复低效的开发。

<!--more-->

现拟引入 openresty 做代理web前端，基于 openresty 的 timer 对后端的 http 服务(以及多种网络服务)做监控，再收集信息对 etcd 服务器统一发起服务注册及保持心跳。

![](https://img.ogura.io/files/2d6b8f38648568bf94e076edc8f7cd6d)
**示例图**

项目地址：<https://github.com/xiaocang/lua-resty-etcd-discovery-client>
