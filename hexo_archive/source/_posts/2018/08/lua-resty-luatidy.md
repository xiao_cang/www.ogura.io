---
title: Lua 自动缩进处理的单文件版本
date: 2018-08-02 16:31:08
tags: lua
---

在 `https://www.oschina.net/code/snippet_563463_19381` 中看到一个使用 Perl 正则对 Lua 文件中进行自动缩进的脚本，感觉思路不错，单文件就可以做到简单的自动的缩进判断。
可是在 github 上搜索了半天，也没能找到相关的 lua standalone 项目代码。

这里顺便吐槽一下 `LuaRocks`，如果要使用 `LuaRocks` 安装好的脚本，机器上就得安装 `LuaRocks` 才行，于是就算打包的时候调用 `LuaRocks` 将依赖解决好，也是不可用的，除非在依赖项中写上 `LuaRocks`。对我这种 standalone 洁癖来说非常不友好。

这里我对 Perl 版本的自动缩进改写成了 Lua 版的，其中的正则使用的是 openresty 中的 `ngx.re`，启动时要调用 `resty` 命令行或者在 `openresty` 中使用 (两种启动方式)

<https://github.com/xiaocang/lua-resty-luatidy>
