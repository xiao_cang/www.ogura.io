---
title: DNS 基础知识
date: 2018-08-30 22:10:05
tags: dns
---

本文主要内容是 [RFC 1035](https://www.ietf.org/rfc/rfc1035.txt) 的解读。

<!-- toc -->

Zone
====

DNS 的区域 (zone) 由两部分组成：Resource Records (RRs) 和 Directives

``` zone
; zone file for example.com
$TTL 2d    ; 172800 secs default TTL for zone
$ORIGIN example.com.
@             IN      SOA   ns1.example.com. hostmaster.example.com. (
                        2003080800 ; se = serial number
                        12h        ; ref = refresh
                        15m        ; ret = update retry
                        3w         ; ex = expiry
                        3h         ; min = minimum
                        )
              IN      NS      ns1.example.com.
              IN      MX  10  mail.example.net.
joe           IN      A       192.168.254.3
www           IN      CNAME   joe
```

<!-- more -->

1. 一个DNS区域文件由注释、指令(Directives) 和记录（RRs）组成
2. 注释是由 ; 开头直至行尾
3. 指令(Directives) 是由 `$` 开头的，其中 `$ORIGIN` 和 `$INCLUDE` 是在 [RFC 1035](https://www.ietf.org/rfc/rfc1035.txt) 中定义的，而 `$GENERATE` 则是由 `BIND` 提供的非标准指令。
4. `$TTL` 指令需要出现在第一个 RR 之前
5. 第一个出现的 RR 必须是 SOA (Start of Authority)

DNS Message
===========

这里的消息是指在 `Resolver` 和 `DNS` 系统之间的消息协议。

格式如下：

```
    +---------------------+
    |        Header       |
    +---------------------+
    |       Question      | the question for the name server
    +---------------------+
    |        Answer       | RRs answering the question
    +---------------------+
    |      Authority      | RRs pointing toward an authority
    +---------------------+
    |      Additional     | RRs holding additional information
    +---------------------+
```
(图自 RFC-1035)

Header
======

```
                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      ID                       |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    QDCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ANCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    NSCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ARCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
```
(图自 RFC-1035)

- ID: 一个16位的请求 ID，响应中原样返回，用于表示请求的唯一性
- QR: 请求响应标识位。请求设置为 0, 响应设置为 1
- OPCODE:
    - 0: QUERY. 标准请求
    - 1: IQUERY. 反向请求 (可选支持)
    - 2: STATUS. DNS 的状态
    - 3-15: 保留
- AA(res only): Authoritative Answer 权威回答. 即zone拥有者返回的响应为权威回答，其他如其他DNS服务器根据缓存给出的响应为非权威回答。
- TC: 截断. 当消息体大于最大可传输大小时即截断消息
- RD: Recursion Desired . 请求消息中要求递归查询位，如果服务器支持递归查询，则置位，否则则置零
- RA(res only): Recursion Available. 该NS服务器是否支持递归查询
- Z: 保留位，在请求和响应中必须都为 0
- RCODE(res only): 标识服务器的响应类型 (类似错误码)
    - 0: 无错误
    - 1: 格式错误: 服务器无法解析请求
    - 2: 服务器错误：由于某种原因导致服务器出错，因此暂时无法响应
    - 3: 对于不支持递归查询的服务器(authority only)没查到相应域名时的响应
    - 4: 未实现：不支持当前的查询类型
    - 5: 拒绝：由于策略或其他原因，拒绝服务
- QDCOUNT: 16位无符号整型，用于标识 Question Section 条目数量
- ANCOUNT: 16位无符号整型，用于标识 Answer Section 中 RR 条目数量
- NSCOUNT: 16位无符号整型，用于标识 Authority Section 的条目数量
- ARCOUNT: 16位无符号整型，用于标识 Additional Section 的条目数量

Question
========

```
                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                                               |
    /                     QNAME                     /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     QTYPE                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     QCLASS                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
```
(图自 RFC-1035)

说明：每个请求通常只有一个 `Question Section`，但其实可以通过 `QDCOUNT` 来指定任意个数的 `Question Section`

- QNAME: 查询的域名。格式为：`no. of chars` `domain name` `no. of chars` `domain name` ...
	其中 `no. of chars` 为相邻 `domain name` 字符串长度
	例：
```
08 6D 79 64 6F 6D 61 69 6E 03 63 6F 6D 00
// printable
 !  m  y  d  o  m  a  i  n  !  c  o  m  !
// note ! = unprintable
```
(图自zytrax.open)
- QTYPE: 查询的类型。即对应 `RR` 的 `TYPE`
- QCLASS: 查询的类(Class)。最常用的值为 `x'0001` 代表 `IN or Internet`

Answer
======

> Answer / Authority / Additional Section / RR 都采用相同的格式

RR / Answer 格式:

```
                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                                               |
    /                                               /
    /                      NAME                     /
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      TYPE                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     CLASS                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      TTL                      |
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                   RDLENGTH                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
    /                     RDATA                     /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
```
(图自 RFC-1035)

- **NAME**: 响应的域名.
    - 格式一：label 格式。同上述 QNAME
    - 格式二：Pointer 格式。经过数据压缩的格式。一个十六位的值: 前两位固定为 `1` (与 label 格式区分，由于 lable 格式最大值限制为 63)，`OFFSET` 位的值为相对于信息开始位置的偏移量。其中 0 代表着 ID 的第一位。
```
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    | 1  1|                OFFSET                   |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
```
(图自 RFC-1035)
- **TYPE**: RR 的类型。
	- `x'0001(1)`: A 记录
	- `x'0002(2)`: NS 记录
	- `x'0005(5)`: CNAME 记录
	- `x'0006(6)`: SOA 记录
	- `x'000B(11)`: WKS 记录 -- Well Known Source 用来描述互联网上的使用特定协议(如`TCP(6)`) [RFC1010](https://www.ietf.org/rfc/rfc1010.txt)的通用服务(如 `SMTP`)
	- `x'000C(12)`: PTR 记录. A 记录与 AAAA 记录的反向记录（IP指向域名）
	- `x'000F(15)`: MX 记录. `SMTP` 的 `Agent` 用来收件的域名
	- `x'0021(33)`: SRV 记录. [RFC 2782](https://www.ietf.org/rfc/rfc2782.txt) MX 记录是其特殊情况。SRV 记录是用来被其他特定服务使用的记录字段(如 OpenLDAP)
	- `x'001C(28)`: AAAA 记录. ipv6 地址
- **CLASS**: RR 的类。如：`Internet` [`Chaos`](https://en.wikipedia.org/wiki/Chaosnet)
- **TTL**: 记录应被缓存时间(秒)
- **RDLENGTH**: RDATA 的长度
- **RDATA**: 每种不同类型的RR数据有特定的格式。
	- SOA: SOA 记录控制着域名记录的更新策略
```
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    /                     MNAME                     /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    /                     RNAME                     /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    SERIAL                     |
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    REFRESH                    |
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     RETRY                     |
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    EXPIRE                     |
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    MINIMUM                    |
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
```
		- Primary NS: 主 NS 服务器. 长度可变. label/pointer/混合
		- Admin MB: 管理员邮箱. 长度可变. label/pointer/混合
		- Serial Number: 序列号. 32位无符号整型. 格式为 "YYYYMMDDnn"
		- Refresh interval: 刷新间隔. 32位无符号整型. 二级NS服务器检查 zone file 的更新的间隔
		- Retry interval: 重试间隔. 32位无符号整型. 当主 NS 服务器无法连接时，重试间隔.
		- Expiration Limit: 过期限制. 32位无符号整型. DNS resolver 可缓存时长，对于某些 DNS server 来说则是对 resolver 响应的缓存时长
		- Minimum TTL: 32位无符号整型. 字段意义取决于 NS 的实现，有以下三种可能：
			- NS 对该域名最小缓存时长，几乎没有服务器这样使用(官方遗弃)
			- 默认的 TTL 值。（无 TTL 记录时使用该值）
			- 定义了当该域无记录时，缓存的时长（区别于有记录时缓存的时长 `TTL`）[RFC 2308](https://www.ietf.org/rfc/rfc2308.txt) (官方推荐)
    - MX:
```
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                  PREFERENCE                   |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    /                   EXCHANGE                    /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
```
		- PREFERENCE: 优先级. 值越小优先级越高. 一般使用 `0`(max)作为邮件服务器记录，使用 `10` 用来验证域名的所有权
		- Mail Exchanger: 提供服务的域名. 长度可变. label/pointer/混合
    - A: 32位无符号整型，IP地址
    - AAAA: 16个八进制，IPv6地址
    - PTR, NS: 地址. label/pointer/混合
- **Authority**: (res only) 在请求中该字段值为 0. 格式同 `Answer`. 数据一般为 `NS` 类型的 `RR`
- **Additional**: (res only) 在请求中该字段值为 0. 格式同 `Answer`. 理论上，任意类型的 `RR` 都是合法的。实际上，此字段用于提供在 `Authority Section` 中提到的 `NS` 域名所对应的 `A` 或者 `AAAA` 记录

	注:
		其中 `(res only)` 代表着仅在DNS响应中有效的字段

参考文章：
- Chapter 8. DNS Resource Records (RRs) <http://www.zytrax.com/books/dns/ch8/>
- Chapter 15 DNS Messages <http://www.zytrax.com/books/dns/ch15/>
- Chaosnet <https://en.wikipedia.org/wiki/Chaosnet>
- DNS: Understanding The SOA Record <http://www.peerwisdom.org/2013/05/15/dns-understanding-the-soa-record/>
- MX record <https://en.wikipedia.org/wiki/MX_record#Priority>
