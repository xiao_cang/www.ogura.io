---
title: Blogroll
date: 2018-09-27 11:15:55
---

- <http://yihui.name/>: 谢益辉的个人主页。R-Project 专家，博客文章有趣又有益。
- <http://www.yinwang.org/>: 王垠的个人主页。语言方面专家，思路开阔。
- <https://fzheng.me/>: 无求斋备笔记。机器人、计算机视觉研究者，编程爱好者。

Friends
=======

- <https://blessing.studio/>: Blessing Studio。半吊子全栈开发者的日常。
- <https://nachtzug.xyz/>: Nachtzug 「夜行列車」
- <https://blog.rexskz.info/>: 音游狗、安全狗、攻城狮、业余设计师、段子手、苦学日语的少年。
