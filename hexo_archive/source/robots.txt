User-agent: *
Allow: /
Allow: /archives/
Allow: /tags/
Allow: /posts/

Disallow: /assets/
Disallow: /lib/
Disallow: /js/
Disallow: /css/
Disallow: /fonts/
Disallow: /scripts/
Disallow: /favicon/
Disallow: /fancybox/

Sitemap: https://www.ogura.io/sitemap.xml
Sitemap: https://www.ogura.io/baidusitemap.xml
